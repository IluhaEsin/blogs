﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Blogs.DAL.Entities;
using Blogs.Models;

namespace Blogs
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateRecordToRecordModelMap();
            CreateRecordCreateModelToRecord();
        }

        public void CreateRecordToRecordModelMap()
        {
            CreateMap<Record, RecordModel>()
                .ForMember(target => target.DatePublished,
                    src => src.MapFrom(p => p.DatePublished.ToString("D")))
                .ForMember(target => target.AuthorName,
                    src => src.MapFrom(p => p.Author.UserName))
                .ForMember(target => target.ContentPreview,
                    src => src.MapFrom(p => p.Content.Substring(0, 20)));
        }

        public void CreateRecordCreateModelToRecord()
        {
            CreateMap<RecordCreateModel, Record>()
                .ForMember(target => target.DatePublished,
                    src => src.MapFrom(p => DateTime.Now));
        }
    }
}
