﻿using System;
using System.Collections.Generic;

namespace Blogs.Models
{
    public class RecordIndexModel
    {
        public string Author { get; set; }
        public string SearchKey { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public List<RecordModel> Records { get; set; }
        public int? Page { get; set; }
        public PagingModel PagingModel { get; set; }
    }
}
