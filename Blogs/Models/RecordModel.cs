﻿using System.ComponentModel.DataAnnotations;

namespace Blogs.Models
{
    public class RecordModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации")]
        public string DatePublished { get; set; }

        [Display(Name = "Автор")]
        public string AuthorName { get; set; }

        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Содержание")]
        public string ContentPreview { get; set; }
    }
}
