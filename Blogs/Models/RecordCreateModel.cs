﻿using System.ComponentModel.DataAnnotations;

namespace Blogs.Models
{
    public class RecordCreateModel
    {
        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Содержание")]
        public string Content { get; set; }
    }
}
