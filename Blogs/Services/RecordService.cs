﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Blogs.DAL.Entities;
using Blogs.DAL.Repositories.Contracts;
using Blogs.Models;
using Blogs.Services.Contracts;

namespace Blogs.Services
{
    public class RecordService : IRecordService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RecordService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<RecordModel> GetAllRecords(RecordIndexModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var records = unitOfWork.Records.GetAllWithAuthors();
                
                records = records
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author)
                    .ByDateFrom(model.DateFrom)
                    .ByDateTo(model.DateTo);

                int pageSize = 2;
                int count = records.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                records = records.Skip((page - 1) * pageSize).Take(pageSize);
                model.PagingModel = new PagingModel(count, page, pageSize);
                model.Page = page;

                var models = Mapper.Map<List<RecordModel>>(records.ToList());

                return models;
            }
        }

        public void CreateRecord(RecordCreateModel model, int currentUserId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var record = Mapper.Map<Record>(model);
                record.AuthorId = currentUserId;

                unitOfWork.Records.Create(record);
            }
        }
    }
}
