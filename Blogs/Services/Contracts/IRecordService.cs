﻿using System.Collections.Generic;
using Blogs.Models;

namespace Blogs.Services.Contracts
{
    public interface IRecordService
    {
        List<RecordModel> GetAllRecords(RecordIndexModel model);
        void CreateRecord(RecordCreateModel model, int currentUserId);
    }
}
