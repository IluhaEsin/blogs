﻿using System;

namespace Blogs.DAL.Entities
{
    public class Record : IEntity
    {
        public int Id { get; set; }

        public DateTime DatePublished { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
    }
}
