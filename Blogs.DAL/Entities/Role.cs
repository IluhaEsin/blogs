﻿using Microsoft.AspNetCore.Identity;

namespace Blogs.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
