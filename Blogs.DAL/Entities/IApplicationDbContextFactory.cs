﻿namespace Blogs.DAL.Entities
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}
