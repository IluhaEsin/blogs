﻿using System;
using System.Collections.Generic;
using System.Text;
using Blogs.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blogs.DAL.EntitiesConfiguration
{
    public class RecordConfiguration : BaseEntityConfiguration<Record>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Record> builder)
        {
            builder
                .Property(b => b.Title)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Content)
                .HasMaxLength(int.MaxValue)
                .IsRequired();

            builder
                .Property(b => b.DatePublished)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Record> builder)
        {
            builder
                .HasOne(b => b.Author)
                .WithMany(b => b.Records)
                .HasForeignKey(b => b.AuthorId)
                .IsRequired();
        }
    }
}
