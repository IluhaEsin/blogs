﻿using Blogs.DAL.Entities;

namespace Blogs.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Record> RecordConfiguration { get; }
    }
}