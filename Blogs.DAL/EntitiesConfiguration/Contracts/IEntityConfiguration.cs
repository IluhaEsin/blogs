﻿using System;
using Blogs.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Blogs.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
