﻿using Blogs.DAL.Entities;
using Blogs.DAL.EntitiesConfiguration.Contracts;

namespace Blogs.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Record> RecordConfiguration { get; }

        public EntityConfigurationsContainer()
        {
            RecordConfiguration = new RecordConfiguration();
        }
    }
}
