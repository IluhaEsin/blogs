﻿using System.Collections.Generic;
using Blogs.DAL.Entities;

namespace Blogs.DAL.Repositories.Contracts
{
    public interface IRecordRepository : IRepository<Record>
    {
        IEnumerable<Record> GetAllWithAuthors();
    }
}
